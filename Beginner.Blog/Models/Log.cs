﻿using Beginner.Blog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Beginner.Blog.Models
{
    public class Log : BaseEntity
    {
        public string Message { get; set; }
        public DateTime CreateTime { get; set; }
    }
}